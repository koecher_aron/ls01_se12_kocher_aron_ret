import java.util.Scanner;
public class Quadrieren {
   
	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
		System.out.println("Bitte geben Sie die Zahl ein: ");
		double x = sc.nextDouble();
				
		// (V) Verarbeitung
		// ================================
		double ergebnis = Quadrieren.quadrat(x);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
	}	
	
	public static double quadrat(double x) {
	double ergebnis= x * x;
	return ergebnis;
}
}