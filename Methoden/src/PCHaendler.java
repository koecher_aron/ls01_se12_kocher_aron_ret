import java.util.Scanner;
public class PCHaendler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Scanner myScanner = new Scanner(System.in);

		int anzahl;
		String artikel;
		double nettopreis, mwst, nettogesamtpreis, bruttogesamtpreis;

		artikel = liesString(myScanner);
		anzahl = liesInt(myScanner);
		System.out.println("Geben Sie den Nettopreis ein:");
		nettopreis = liesDouble(myScanner);
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		mwst = liesDouble(myScanner);
			
		
		nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
		bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		ausgabe(anzahl, artikel, nettogesamtpreis, bruttogesamtpreis, mwst);
		
		
	}
		
		// Benutzereingaben lesen
	
		//Eingabe
		public static String liesString(Scanner myScanner) {
	    	System.out.println("was moechten Sie bestellen?");
	    	String partikel = myScanner.next();
	    	return partikel;
	    }
  
				
		public static int liesInt(Scanner myScanner) {
			System.out.println("Geben Sie die Anzahl ein:");
			int panzahl = myScanner.nextInt();
			return panzahl;
		}
		
		
		public static double liesDouble(Scanner myScanner) {
		double p = myScanner.nextDouble();
		return p;
		}

		
		public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
			double nettogesamt = anzahl * nettopreis;
			return nettogesamt;
		}
			
		public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
			double bruttogesamt = nettogesamtpreis * (1 + mwst / 100);
			return bruttogesamt;
		}
		
		
		public static void ausgabe(int anzahl, String artikel, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		} 
	}

