﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	
    	
    	//E
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rueckgabebetrag;
               
      
      zuZahlenderBetrag = fahrkartenEingabe(tastatur); 

       rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

       fahrscheinAusgabe();
       
       rueckgeldAusgabe(rueckgabebetrag);      
    
	}
    
    
    
    
    public static void rueckgeldAusgabe(double rueckgabebetrag) {
        if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f %s \n " , rueckgabebetrag , " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rueckgabebetrag -= 0.05;
            } 
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
        }
    
    
    
    
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f %s \n"  , (zuZahlenderBetrag - eingezahlterGesamtbetrag) , "EURO");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextFloat();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        double rueckgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rueckgeld;
    }
        
        
        
    
    public static void fahrscheinAusgabe(){
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
        }
    
    
    

	public static double fahrkartenEingabe(Scanner tastatur) {
		System.out.println("Ticketkosten:");
		double zuZahlenderBetrag = tastatur.nextDouble();
		System.out.println("Anzahl der Tickets:");
		int anzahlt = tastatur.nextInt();
		zuZahlenderBetrag *= anzahlt;
		return  zuZahlenderBetrag;
	}
}