import java.util.Scanner;
public class Addition {
	
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		double  erg,zahl1, zahl2;

        //1.Programmhinweis
		Addition.programmhinweis();
       

        //4.Eingabe
		System.out.println(" 1. Zahl: ");
        zahl1 = Addition.eingabe();
        System.out.println(" 2. Zahl: ");
		zahl2 = Addition.eingabe();

        //3.Verarbeitung		
        erg = Addition.addieren(zahl1, zahl2);
        
        
        //2.Ausgabe
       Addition.ausgabe(zahl1, zahl2, erg);
		
		
		
		
	}
		//Mehtoden
	//programmhinweis
	public static void programmhinweis() {
	 System.out.println("Hinweis: ");
     System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
	}
	
	
	
	//eingabe
	public static double eingabe() {
	double pzahl1 = sc.nextDouble();
	 return pzahl1;
	}
	

	 
	 
	 //verarbeitung
	 public static double addieren(double pzahl1, double pzahl2) {
		double perg = pzahl1 + pzahl2;
		return perg;
	 }
	 
	 	 
	 
	 //ausgabe
	 public static void ausgabe(double zahl1, double zahl2, double erg) {
	 System.out.println("Ergebnis der Addition");
     System.out.printf("%.2f = %.2f + %.2f", erg, zahl1, zahl2);
	 }

}
