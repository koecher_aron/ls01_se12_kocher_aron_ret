import java.util.Scanner;
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
	  Scanner tastatur = new Scanner(System.in);
      double x, y, m;

      System.out.println("Bitte geben Sie die erste Zahl ein: ");
      x = tastatur.nextDouble();
      System.out.println("Bitte geben Sie die zweite Zahl ein: ");
      y = tastatur.nextDouble();
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
     m = Mittelwert.berechnung(x,y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
   
   public static double berechnung(double x,double y){
   double  m = (x + y) / 2.0;
    return m;
   }
    
   
   
   
   
   
}