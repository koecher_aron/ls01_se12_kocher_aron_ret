﻿import java.util.Scanner;

class Fahrkartenautomat {
    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);

        double zuZahlenderBetrag;
        double rueckgabebetrag;
        

        

        
        do {
        //Fahrkartenbestellung erfassen
        
        zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
        

        // Geldeinwurf  (fahrkartenBezahlen(...))
        // -----------
        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
        
        
        // Fahrscheinausgabe
        // -----------------
        fahrkartenAusgabe();
        


        // R�ckgeldberechnung und -Ausgabe     rueckgeldAusgaben(....)
        // -------------------------------
        rueckgeldAusgeben(rueckgabebetrag);
        
        }while(true); //do schleife- ende
       
    }   //Ende dee Main
    
    
    
    
    
    
    
    
    
    // Fahrscheinausgabe
    // -----------------
    public static void fahrkartenAusgabe() {
    System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 8; i++) {
        System.out.print("=");
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    System.out.println("\n\n");
    }
    
    // Fahrkartenbestellung erfassen
    // -----------------
    public static double fahrkartenbestellungErfassen(Scanner tastatur) {
        
        String verzeichnis[] = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
        double preise[] = {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
        
       	boolean eingabe = true;
    	int indexx = 0;  //java heuklt schon wieder rum das irgendwo die initializierung fehlen könnte
    	double ticketPreis = 0; // musste den Wert inizialisieren, da ich mit den kommenden schleifen den Wert nichtmher konstant vor Berechnung belege (glaube ich)
    	double anzahlTickets;
    	double zuZahlenderBetrag;
    	
    	System.out.println("Wählen Sie ihre Wunschfahrkarte aus: ");
    	for(int i = 0; i < verzeichnis.length; i++) {
    		System.out.printf("%-40s %6.2f %d \n", verzeichnis[i] , preise[i] , i+1);
    	}
    	System.out.println("Bitte wählen Sie ihr Ticket");
    		
        while(eingabe == true) {
        	indexx = tastatur.nextInt() - 1;
        	if(indexx >10 || indexx <= 0) {
        	System.out.println("Ihre Eingabe wurde nicht erkannt, bitte versuchen <sie es erneut.");
        	
        	}
        	else {
        		eingabe = false;
        	}
        }
        	
              
        

        
        
       
        do{
        	System.out.print("Anzahl der Tickets: ");
        	anzahlTickets = tastatur.nextInt();
        	System.out.println("Die Anzhal der Tickets war negativ oder zu groß! Bitte geben Sie einen Wert von 1-10 an.");
        
        }while(anzahlTickets > 10 || anzahlTickets <= 0);      
          zuZahlenderBetrag = preise[indexx] * anzahlTickets;	
          return  zuZahlenderBetrag;	
        
      }   

    
   
    
    // Geldeinwurf  (fahrkartenBezahlen(...))
    // -----------
    public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMuenze;
    	double rueckgeld;
        eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f €%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
    	
    	rueckgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	
    	return rueckgeld;
    }
    
    public static void rueckgeldAusgeben(double rueckgabebetrag) { 
        if (rueckgabebetrag > 0.0) {
            System.out.format("Der Rückgabebetrag in Höhe von %4.2f € %n", rueckgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir wünschen Ihnen eine gute Fahrt.\n");
       }
    
}